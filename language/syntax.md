# Syntax

The syntax of Pop will be described as follows:

`name = parsling_1, parsling_2? : "parsling_3";`

- `name` is where the name of the rule goes.
- A parsling_N is a name given to rules (does not have to start with `parsling_`).
- Question marks are optional parslings, there can be no optional lexemes.
- Quotation marks means that the parsling must instead be a lexeme type.
- Commas are separators between parslings / lexemes in a single option group.
- Colons are separators between possible option groups.

## Rules

```
file = declarations;
declarations = declaration, declarations;
declaration = name, "colon", type, "equals", $declaration_body_type, (function_declaration);

name = "identifier";

type = "identifier";

function_declaration: argument_list, argument_list, scope;
argument_list: "left_parenthesis", "right_parenthesis";

scope = "left_curly_bracket", statements, "right_curly_bracket";
statements = statement, statements;
    
statement = $statement_type, (function_call_statement);
    
function_call_statement = name, "left_parenthesis", "right_parenthesis", "semi_colon";
```

# Abstract Syntax

The abstract syntax of Pop will be described as follows:

## Rules

```
file = containers;

containers = #container, [container];

container = declaration : action;

declaration = name, type?, arguments?, arguments?, tags?, expression?, containers?;

name = "name";

type = "type";

arguments;

tags;

action = function_call;

expression;
```