# Philosophy

Pop aims to a be one main thing:

Fast.

DISCLAIMER: Pop is not in a working state at the time of writing this message. This is more of a goal list than a feature list.

## Fast In What Ways?

- Compile Time
- Run Time
- Unit Testing
- Refactoring

## The Syntax Is Simple

Thus the compiler code stays small & efficient.

Example Main Function:

```
main := ()() {
    return();
}
```

## Executables Will Be Lightweight

- Bounds checking is non-existent.
- There is a minimal standard library.
- Unused code is not included in the final executable.
- Executables will be built from scratch each time.
- There are no external standard libraries to link to during runtime.
