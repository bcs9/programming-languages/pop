# Pop Programming

Pop is a data oriented programming language designed at getting from point A to point B as efficiently as possible.

## Compilation

Release

`make release`

Debug

`make debug` or `make`

## Goals

First Steps:

- [x] Loading Files
- [x] Deserializing UTF8
- [x] Basic Lexical Analysis
- [ ] Basic Parsing
- [ ] Basic Abstracting
- [ ] Basic Intermediating

Basic Boilerplate Features:

- [ ] Program Quits
- [ ] Function Calls
- [ ] Variables
- [ ] Mathematics
- [ ] Pointers
- [ ] Structures
- [ ] Linux System Calls
- [ ] Importing Files

Advanced Boilerplate Features:

- [ ] Type Inference
- [ ] Function Overloading