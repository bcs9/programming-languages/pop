#include <stdio.h>
#include <stdlib.h>

/* Defines */
#define POP_ERROR_TRACE_LIMIT 8

#define POP_FALSE 0
#define POP_TRUE 1

typedef struct POP_parsling__statements POP_parsling__statements;
typedef struct POP_parsling__declarations POP_parsling__declarations;

/* Types */
// error type
typedef enum POP_et {
    // none
    POP_et__no_error,

    // loading
    POP_et__loading_file_not_found,
    POP_et__loading_file_larger_than_2GB,
    POP_et__loading_file_could_not_be_fully_read,

    // extracting
    POP_et__extracting_invalid_uft8,

    // lexing
    POP_et__lexing_lexeme_larger_than_2GB,
    POP_et__lexing_non_ascii_characters_outside_of_string,
    POP_et__lexing_incomplete_string_literal,

    // parsing
    POP_et__parsing_incomplete_declaration,
    POP_et__parsing_incomplete_name,
    POP_et__parsing_incomplete_type,
    POP_et__parsing_incomplete_scope,
    POP_et__parsing_incomplete_argument_list,
    POP_et__parsing_incomplete_function_call_statement,

    // abstracting
    POP_et__abstracting_max_scope_depth_reached,
} POP_et;

// jump type
typedef enum POP_jt {
    POP_jt__double_quote,
    POP_jt__left_parenthesis,
    POP_jt__right_parenthesis,
    POP_jt__comma,
    POP_jt__period,
    POP_jt__forward_slash,
    POP_jt__digit,
    POP_jt__identifier,
    POP_jt__colon,
    POP_jt__semi_colon,
    POP_jt__equals,
    POP_jt__left_curly_bracket,
    POP_jt__right_curly_bracket,
    POP_jt__error,
    POP_jt__unused,
} POP_jt;

// lexeme type
typedef enum POP_lt {
    POP_lt__identifier,
    POP_lt__string_literal,
    POP_lt__integer_literal,
    POP_lt__colon,
    POP_lt__semi_colon,
    POP_lt__equals,
    POP_lt__period,
    POP_lt__division,
    POP_lt__comma,
    POP_lt__left_parenthesis,
    POP_lt__right_parenthesis,
    POP_lt__left_curly_bracket,
    POP_lt__right_curly_bracket,
    POP_lt__eof,
    POP_lt__error
} POP_lt;

// parsling type
typedef enum POP_pt {
    POP_pt__file,
    POP_pt__declarations,
    POP_pt__declaration,
    POP_pt__name,
    POP_pt__type,
    POP_pt__declaration_body,
    POP_pt__function_declaration,
    POP_pt__argument_list,
    POP_pt__scope,
    POP_pt__statements,
    POP_pt__statement,
    POP_pt__function_call_statement,
    POP_pt__error
} POP_pt;

// parsling action type
typedef enum POP_pat {
    POP_pat__error = 0x0,
    POP_pat__increment_output_member_pointer = 0x1,
    POP_pat__call_function = 0x2,
    POP_pat__return_from_function = 0x4,
    POP_pat__write_lexeme = 0x8,
    POP_pat__write_return_value = 0x10,
    POP_pat__allocate_new_block = 0x20,
    POP_pat__quit = 0x40,
} POP_pat;

typedef enum POP_ptt {
    POP_ptt__error,
    POP_ptt__parameterized_scope,
    POP_ptt__function_call
} POP_ptt;

typedef struct POP_parsling__name {
    unsigned long long p_name_lexeme_index;
} POP_parsling__name;

typedef struct POP_parsling__type {
    POP_parsling__name* p_type_name;
} POP_parsling__type;

typedef struct POP_parsling__argument_list {
    unsigned long long p_left_parenthesis_lexeme_index;
    unsigned long long p_right_parenthesis_lexeme_index;
} POP_parsling__argument_list;

typedef struct POP_parsling__function_call {
    POP_parsling__name* p_name;
    unsigned long long p_left_parenthesis_lexeme_index;
    unsigned long long p_right_parenthesis_lexeme_index;
    unsigned long long p_semi_colon_lexeme_index;
} POP_parsling__function_call;

typedef struct POP_parsling__statement {
    POP_ptt p_statement_type;
    void* p_statement_data;
} POP_parsling__statement;

typedef struct POP_parsling__statements {
    POP_parsling__statements* p_statements;
    POP_parsling__statement* p_statement;
} POP_parsling__statements;

typedef struct POP_parsling__scope {
    unsigned long long p_left_curly_bracket_lexeme_index;
    POP_parsling__statements* p_statements;
    unsigned long long p_right_curly_bracket_lexeme_index;
} POP_parsling__scope;

typedef struct POP_parsling__declaration_body {
    POP_parsling__argument_list* p_inputs;
    POP_parsling__argument_list* p_outputs;
    POP_parsling__scope* p_scope;
} POP_parsling__declaration_body;

typedef struct POP_parsling__declaration {
    POP_parsling__name* p_name;
    unsigned long long p_colon_lexeme_index;
    POP_parsling__type* p_type;
    unsigned long long p_equals_lexeme_index;
    POP_ptt p_declaration_body_type;
    void* p_declaration_body;
} POP_parsling__declaration;

typedef struct POP_parsling__declarations {
    POP_parsling__declarations* p_declarations;
    POP_parsling__declaration* p_declaration;
} POP_parsling__declarations;

typedef struct POP_parsling__file {
    POP_parsling__declarations* p_declarations;
} POP_parsling__file;

typedef struct POP_parsling_stack_item {
    unsigned long long p_sub_instruction_pointer;
    void* p_output_base_pointer;
    POP_pt p_instruction;
} POP_parsling_stack_item;

typedef struct POP_parsling_stack {
    unsigned long long p_max_height;
    unsigned long long p_height;
    POP_parsling_stack_item* p_stack;
} POP_parsling_stack;

// abstractling type
typedef enum POP_at {
    POP_at__file,
    POP_at__container,
    POP_at__declaration,
    POP_at__name,
    POP_at__type,
    POP_at__action
} POP_at;

// abstractling action type
typedef enum POP_aat {
    POP_aat__error = 0x0,
    POP_aat__quit = 0x1
} POP_aat;

typedef struct POP_buffer {
    unsigned int p_length;
    unsigned char* p_data;
} POP_buffer;

typedef struct POP_error_log {
    POP_et p_error_trace[POP_ERROR_TRACE_LIMIT];
    
    // parsing
    unsigned long long p_parsing_lexeme_index;
    POP_lt p_parsing_missing_lexeme;
} POP_error_log;

typedef struct POP_code_points {
    unsigned int p_length;
    int* p_points;
} POP_code_points;

typedef struct POP_lexemes {
    unsigned int p_length;
    unsigned int* p_lexeme_indices;
    unsigned int* p_lexeme_lengths;
    POP_lt* p_lexeme_types;
} POP_lexemes;

typedef struct POP_parslings {
    unsigned long long p_data_length;
    unsigned long long p_fill_length;
    unsigned long long* p_data;
} POP_parslings;

typedef struct POP_abstractlings {
    unsigned long long p_data_length;
    unsigned long long p_fill_length;
    unsigned long long* p_data;
} POP_abstractlings;

/* Error Handling */
POP_error_log POP_initialize_error_log() {
    POP_error_log output;

    // setup error trace
    for (unsigned int i = 0; i < POP_ERROR_TRACE_LIMIT; i++) {
        output.p_error_trace[i] = POP_et__no_error;
    }

    // setup parsing vairables
    output.p_parsing_missing_lexeme = POP_lt__error;
    output.p_parsing_lexeme_index = 0;

    return output;
}

unsigned char POP_error_occured(POP_error_log error_log) {
    for (unsigned int i = 0; i < POP_ERROR_TRACE_LIMIT; i++) {
        if (error_log.p_error_trace[i] != POP_et__no_error) {
            return POP_TRUE;
        }
    }

    return POP_FALSE;
}

void POP_print_error_log(POP_error_log error_log) {
    printf("Pop Error: \nTrace:");

    for (unsigned long long i = 0; i < POP_ERROR_TRACE_LIMIT; i++) {
        printf("\tError: %llu\n", (unsigned long long)error_log.p_error_trace[i]);
    }

    printf("Missing Lexeme: %llu\nLexeme Index: %llu\n", (unsigned long long)error_log.p_parsing_missing_lexeme, (unsigned long long)error_log.p_parsing_lexeme_index);

    return;
}

/* Library Functions */
void POP_print_tabs(unsigned long long depth) {
    while (depth > 0) {
        printf(" ");
        depth--;
    }

    return;
}

unsigned int POP_c_string_length(char* string) {
    unsigned int length = 0;

    while (string[length] != 0) {
        length++;
    }

    return length;
}

POP_buffer POP_initialize_buffer(unsigned int length) {
    POP_buffer output;

    output.p_length = length;
    output.p_data = (unsigned char*)malloc(sizeof(unsigned char) * length);

    return output;
}

POP_buffer POP_copy_string_to_buffer(char* string) {
    POP_buffer output;

    output = POP_initialize_buffer(sizeof(unsigned char) * POP_c_string_length(string));

    for (unsigned int i = 0; i < output.p_length; i++) {
        output.p_data[i] = string[i];
    }

    return output;
}

void POP_print_buffer(POP_buffer buffer) {
    for (unsigned int i = 0; i < buffer.p_length; i++) {
        printf("%c", buffer.p_data[i]);
    }
    printf("\n");
    fflush(stdout);
}

void POP_destroy_buffer(POP_buffer buffer) {
    free(buffer.p_data);

    return;
}

/* Loading */
POP_buffer POP_load_file_into_buffer(POP_et* error, char* file_address) {
    POP_buffer output;
    long long length;
    FILE* file_handle;

    // zero output to correct warning
    output.p_length = 0;

    // open file
    file_handle = fopen((const char*)file_address, "rb");
    if (!file_handle) {
        *error = POP_et__loading_file_not_found;

        return output;
    }

    // get the file length
    fseek(file_handle, 0, SEEK_END);
    length = ftell(file_handle);
    rewind(file_handle);

    // check if file is too large 2GB Max!
    if (length > __INT32_MAX__ - 1) {
        *error = POP_et__loading_file_larger_than_2GB;
        fclose(file_handle);

        return output;
    }

    // initialize output
    output = POP_initialize_buffer(sizeof(unsigned char) * length);

    // read file into output buffer
    length = fread(output.p_data, 1, output.p_length, file_handle);

    // make sure that the entire file was read
    if (length != output.p_length) {
        *error = POP_et__loading_file_could_not_be_fully_read;

        POP_destroy_buffer(output);
        fclose(file_handle);

        return output;
    }

    // close the file
    fclose(file_handle);

    // output is ready to go
    return output;
}

/* Extracting */
void POP_destroy_code_points(POP_code_points code_points) {
    free(code_points.p_points);

    return;
}

unsigned long long POP_get_code_point_length_from_string(unsigned char* string, unsigned int index) {
    if ((string[index] & 0b11111000) == 0b11110000) {
        return 4;
    } else if ((string[index] & 0b11110000) == 0b11100000) {
        return 3;
    } else if ((string[index] & 0b11100000) == 0b11000000) {
        return 2;
    } else if (string[index] < 128) {
        return 1;
    } else {
        return 0;
    }
}

int POP_get_code_point_value(unsigned char* str, unsigned int index, unsigned int* length) {
    int value = 0;

    if ((str[index] & 0b11111000) == 0b11110000) {
        value += ((int)(str[index] & 0b00000111)) << 18;
        value += ((int)(str[index + 1] & 0b00111111)) << 12;
        value += ((int)(str[index + 2] & 0b00111111)) << 6;
        value += (int)(str[index + 3] & 0b00111111);

        *length = 4;
    } else if ((str[index] & 0b11110000) == 0b11100000) {
        value += ((int)(str[index] & 0b00001111)) << 12;
        value += ((int)(str[index + 1] & 0b00111111)) << 6;
        value += (int)(str[index + 2] & 0b00111111);

        *length = 3;
    } else if ((str[index] & 0b11100000) == 0b11000000) {
        value += ((int)(str[index] & 0b00011111)) << 6;
        value += (int)(str[index + 1] & 0b00111111);

        *length = 2;
    } else if (str[index] < 128) {
        value = str[index];

        *length = 1;
    }

    return value;
}

POP_code_points POP_deserialize_utf8(POP_et* error, POP_buffer string) {
    POP_code_points output;
    unsigned int length = 0;
    unsigned int index = 0;
    unsigned int current_point = 0;

    // initialize variables
    output.p_length = 0;
    output.p_points = 0;

    // first pass, determine code point count & validity
    while (index < string.p_length) {
        length = POP_get_code_point_length_from_string(string.p_data, index);

        if (length == 0) {
            *error = POP_et__extracting_invalid_uft8;

            return output;
        }

        index += length;
        current_point++;
    }

    // setup output
    output.p_length = current_point;
    output.p_points = (int*)malloc(sizeof(int) * current_point);

    // reset counters
    index = 0;
    current_point = 0;

    // second pass, get code point values
    while (index < string.p_length) {
        output.p_points[current_point] = POP_get_code_point_value(string.p_data, index, &length);
        index += length;
        current_point++;
    }

    // code points are ready
    return output;
}

void POP_print_code_points(POP_code_points code_points) {
    printf("Code Points:\n");

    for (unsigned int i = 0; i < code_points.p_length; i++) {
        printf("\t%u: %i\n", i, code_points.p_points[i]);
    }

    printf("----------\n");

    return;
}

/* Lexing */
void POP_destroy_lexemes(POP_lexemes lexemes) {
    free(lexemes.p_lexeme_indices);
    free(lexemes.p_lexeme_lengths);
    free(lexemes.p_lexeme_types);

    return;
}

POP_lexemes POP_lex_code_points(POP_et* error, POP_code_points code_points) {
    POP_lexemes output;
    POP_jt character_jump_table[128];
    int max_length = __INT32_MAX__ - 1;
    unsigned int lexeme_count = 0;
    unsigned int index = 0;
    unsigned int length = 0;
    unsigned int depth;
    int point;
    POP_jt jump_type;
    
    // fill character jump table with values
    // fill with unused first
    for (int i = 0; i < 128; i++) {
        character_jump_table[i] = POP_jt__unused;
    }

    // double quote
    character_jump_table[34] = POP_jt__double_quote;

    // parenthesis
    character_jump_table[40] = POP_jt__left_parenthesis;
    character_jump_table[41] = POP_jt__right_parenthesis;

    // comma
    character_jump_table[44] = POP_jt__comma;

    // periods
    character_jump_table[46] = POP_jt__period;

    // division
    character_jump_table[47] = POP_jt__forward_slash;

    // digits
    for (int i = 48; i < 58; i++) {
        character_jump_table[i] = POP_jt__digit;
    }

    // upper case letters
    for (int i = 65; i < 91; i++) {
        character_jump_table[i] = POP_jt__identifier;
    }

    // underscore
    character_jump_table[95] = POP_jt__identifier;

    // lower case letters
    for (int i = 97; i < 123; i++) {
        character_jump_table[i] = POP_jt__identifier;
    }

    // others
    character_jump_table[58] = POP_jt__colon;
    character_jump_table[59] = POP_jt__semi_colon;
    character_jump_table[61] = POP_jt__equals;

    // curly brackets
    character_jump_table[123] = POP_jt__left_curly_bracket;
    character_jump_table[125] = POP_jt__right_curly_bracket;

    // end of ascii
    character_jump_table[127] = POP_jt__error;

    // zero output to correct warning
    output.p_length = 0;

    // get lexeme count
    for (unsigned int i = 0; i < code_points.p_length; i += length) {
        // check to see if previous lexeme was too long
        if (length == max_length) {
            *error = POP_et__lexing_lexeme_larger_than_2GB;

            return output;
        }

        length = 0;
        point = code_points.p_points[i];

        if (point < 128) {
            jump_type = character_jump_table[point];
        } else {
            jump_type = POP_jt__error;
        }

        switch (jump_type) {
        case POP_jt__double_quote:
            // check for a string literal
            do {
                length++;
                point = code_points.p_points[i + length];
            } while (point != 34 && point != 0 && length < max_length);

            // add the end quote to the lexeme
            length++;

            // check to see if the string literal ended abruptly
            if (point == 0 || length == max_length) {
                *error = POP_et__lexing_incomplete_string_literal;
                
                return output;
            }

            lexeme_count++;
            continue;
        case POP_jt__left_parenthesis:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__right_parenthesis:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__comma:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__period:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__forward_slash:
            // check if the lexeme is a comment or a division literal
            // comment (not a lexeme but whitespace)
            if (code_points.p_points[i + 1] == '*') {
                length = 2;
                depth = 1;
                    
                while (depth != 0) {
                    if (code_points.p_points[i + length] == 0 || code_points.p_points[i + length + 1] == 0) {
                        break;
                    }
                    if (code_points.p_points[i + length] == '*' && code_points.p_points[i + length + 1] == '/') {
                        depth--;
                        length++;
                    }
                    if (code_points.p_points[i + length] == '/' && code_points.p_points[i + length + 1] == '*') {
                        depth++;
                        length++;
                    }

                    length++;
                }
            // division literal
            } else {
                length = 1;

                lexeme_count++;
            }

            continue;
        case POP_jt__digit:
            while ((point > 47 && point < 58) && length < max_length) {
                length++;
                point = code_points.p_points[i + length];
            }

            lexeme_count++;
            continue;
        case POP_jt__identifier:
            while (((point > 64 && point < 91) || (point > 96 && point < 123) || (point > 47 && point < 58) || point == 95) && length < max_length) {
                length++;
                point = code_points.p_points[i + length];
            }

            lexeme_count++;
            continue;
        case POP_jt__colon:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__semi_colon:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__equals:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__left_curly_bracket:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__right_curly_bracket:
            length = 1;

            lexeme_count++;
            continue;
        case POP_jt__error:
            *error = POP_et__lexing_non_ascii_characters_outside_of_string;

            return output;
        case POP_jt__unused:
        default:
            length = 1;
            continue;
        }

        break;
    }

    // adjust by 1 for eof lexeme
    lexeme_count++;

    // set output length
    output.p_length = lexeme_count;

    // initialize output
    output.p_lexeme_indices = (unsigned int*)malloc(sizeof(unsigned int) * lexeme_count);
    output.p_lexeme_lengths = (unsigned int*)malloc(sizeof(unsigned int) * lexeme_count);
    output.p_lexeme_types = (POP_lt*)malloc(sizeof(POP_lt) * lexeme_count);

    // get the lexemes
    for (unsigned int i = 0; i < code_points.p_length; i += length) {
        // check to see if previous lexeme was too long
        if (length == max_length) {
            *error = POP_et__lexing_lexeme_larger_than_2GB;

            return output;
        }

        length = 0;
        point = code_points.p_points[i];

        if (point < 128) {
            jump_type = character_jump_table[point];
        } else {
            jump_type = POP_jt__error;
        }

        switch (jump_type) {
        case POP_jt__double_quote:
            // check for a string literal
            do {
                length++;
                point = code_points.p_points[i + length];
            } while (point != 34 && point != 0 && length < max_length);

            // add the end quote to the lexeme
            length++;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__string_literal;
            index++;

            continue;
        case POP_jt__left_parenthesis:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__left_parenthesis;
            index++;

            continue;
        case POP_jt__right_parenthesis:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__right_parenthesis;
            index++;
            
            continue;
        case POP_jt__comma:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__comma;
            index++;
            
            continue;
        case POP_jt__period:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__period;
            index++;
            
            continue;
        case POP_jt__forward_slash:
            // check if the lexeme is a comment or a division literal
            // comment (not a lexeme but whitespace)
            if (code_points.p_points[i + 1] == '*') {
                length = 2;
                depth = 1;
                    
                while (depth != 0) {
                    if (code_points.p_points[i + length] == 0 || code_points.p_points[i + length + 1] == 0) {
                        break;
                    }
                    if (code_points.p_points[i + length] == '*' && code_points.p_points[i + length + 1] == '/') {
                        depth--;
                        length++;
                    }
                    if (code_points.p_points[i + length] == '/' && code_points.p_points[i + length + 1] == '*') {
                        depth++;
                        length++;
                    }

                    length++;
                }
            // division literal
            } else {
                length = 1;

                // write the lexeme
                output.p_lexeme_indices[index] = i;
                output.p_lexeme_lengths[index] = length;
                output.p_lexeme_types[index] = POP_lt__division;
                index++;
            }

            continue;
        case POP_jt__digit:
            while ((point > 47 && point < 58) && length < max_length) {
                length++;
                point = code_points.p_points[i + length];
            }

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__integer_literal;
            index++;
            
            continue;
        case POP_jt__identifier:
            while (((point > 64 && point < 91) || (point > 96 && point < 123) || (point > 47 && point < 58) || point == 95) && length < max_length) {
                length++;
                point = code_points.p_points[i + length];
            }

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__identifier;
            index++;
            
            continue;
        case POP_jt__colon:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__colon;
            index++;
            
            continue;
        case POP_jt__semi_colon:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__semi_colon;
            index++;
            
            continue;
        case POP_jt__equals:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__equals;
            index++;
            
            continue;
        case POP_jt__left_curly_bracket:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__left_curly_bracket;
            index++;
            
            continue;
        case POP_jt__right_curly_bracket:
            length = 1;

            // write the lexeme
            output.p_lexeme_indices[index] = i;
            output.p_lexeme_lengths[index] = length;
            output.p_lexeme_types[index] = POP_lt__right_curly_bracket;
            index++;
            
            continue;
        case POP_jt__unused:
        default:
            length = 1;
            continue;
        }

        break;
    }

    // add the eof lexeme
    output.p_lexeme_indices[output.p_length - 1] = (unsigned int)code_points.p_length;
    output.p_lexeme_lengths[output.p_length - 1] = 0;
    output.p_lexeme_types[output.p_length - 1] = POP_lt__eof;

    // lexemes have been lexed
    return output;
}

void POP_print_lexemes(POP_lexemes lexemes) {
    printf("Lexemes:\n\tID : Index : Length : Type\n");

    for (unsigned int i = 0; i < lexemes.p_length; i++) {
        printf("\t%u : %u : %u : %i\n", i, lexemes.p_lexeme_indices[i], lexemes.p_lexeme_lengths[i], lexemes.p_lexeme_types[i]);
    }

    printf("----------\n");

    return;
}

/* Parsing */
void POP_destroy_parslings(POP_parslings parslings) {
    free(parslings.p_data);

    return;
}

POP_buffer POP_parse_lexemes(POP_error_log* error_log, POP_lexemes lexemes) {
    POP_buffer output;
    POP_parsling_stack stack;
    unsigned long long lexeme_index = 0;
    void* return_value;
    void* output_top;
    unsigned char running = POP_TRUE;

    // setup output
    output.p_length = lexemes.p_length * 5 * sizeof(unsigned long long); // give generous length (temporary)
    output.p_data = (unsigned char*)calloc(output.p_length, sizeof(unsigned char));
    output_top = output.p_data;

    // setup stack
    stack.p_max_height = lexemes.p_length + 1;
    stack.p_height = 0;
    stack.p_stack = (POP_parsling_stack_item*)malloc(sizeof(POP_parsling_stack_item) * stack.p_max_height);

    // setup parser
    stack.p_stack[stack.p_height].p_instruction = POP_pt__file;
    stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
    stack.p_stack[stack.p_height].p_output_base_pointer = output_top;

    // parse
    while (running == POP_TRUE) {
        // do instructions
        switch (stack.p_stack[stack.p_height].p_instruction) {
        case POP_pt__file:
            switch (stack.p_stack[stack.p_height].p_sub_instruction_pointer) {
            // declarations
            case 0:
                // allocate space
                output_top += sizeof(POP_parsling__file);

                // next sub instruction after call
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                // call function
                stack.p_height++;
                stack.p_stack[stack.p_height].p_instruction = POP_pt__declarations;
                stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
                stack.p_stack[stack.p_height].p_output_base_pointer = output_top;
                
                break;
            // return
            default:
                ((POP_parsling__file*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_declarations = (POP_parsling__declarations*)return_value;
                running = POP_FALSE;
                
                continue;
            }
            break;
        case POP_pt__declarations:
            switch (stack.p_stack[stack.p_height].p_sub_instruction_pointer) {
            // declaration
            case 0:
                // allocate space
                output_top += sizeof(POP_parsling__declarations);

                // next sub instruction after call
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                // call function
                stack.p_height++;
                stack.p_stack[stack.p_height].p_instruction = POP_pt__declaration;
                stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
                stack.p_stack[stack.p_height].p_output_base_pointer = output_top;

                break;
            // return
            default:
                ((POP_parsling__declarations*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_declaration = (POP_parsling__declaration*)return_value;
                return_value = stack.p_stack[stack.p_height].p_output_base_pointer;
                stack.p_height--;
                
                continue;
            }
            break;
        case POP_pt__declaration:
            switch (stack.p_stack[stack.p_height].p_sub_instruction_pointer) {
            // name
            case 0:
                // allocate space
                output_top += sizeof(POP_parsling__declaration);

                // next sub instruction after call
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                // call function
                stack.p_height++;
                stack.p_stack[stack.p_height].p_instruction = POP_pt__name;
                stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
                stack.p_stack[stack.p_height].p_output_base_pointer = output_top;

                break;
            // colon
            case 1:
                // get return value
                ((POP_parsling__declaration*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_name = (POP_parsling__name*)return_value;
                
                // check for lexeme
                if (lexemes.p_lexeme_types[lexeme_index] == POP_lt__colon) {
                    ((POP_parsling__declaration*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_colon_lexeme_index = lexeme_index;
                    lexeme_index++;
                } else {
                    // log error
                    error_log->p_error_trace[0] = POP_et__parsing_incomplete_declaration;
                    error_log->p_parsing_missing_lexeme = POP_lt__colon;
                    error_log->p_parsing_lexeme_index = lexeme_index;

                    // quit
                    running = POP_FALSE;
                }

                // next sub instruction
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                break;
            // type
            case 2:
                // next sub instruction after call
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                // call function
                stack.p_height++;
                stack.p_stack[stack.p_height].p_instruction = POP_pt__type;
                stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
                stack.p_stack[stack.p_height].p_output_base_pointer = output_top;

                break;
            // equals
            case 3:
                // get return value
                ((POP_parsling__declaration*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_type = (POP_parsling__type*)return_value;
                
                // check for lexeme
                if (lexemes.p_lexeme_types[lexeme_index] == POP_lt__equals) {
                    ((POP_parsling__declaration*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_equals_lexeme_index = lexeme_index;
                    lexeme_index++;
                } else {
                    // log error
                    error_log->p_error_trace[0] = POP_et__parsing_incomplete_declaration;
                    error_log->p_parsing_missing_lexeme = POP_lt__equals;
                    error_log->p_parsing_lexeme_index = lexeme_index;

                    // quit
                    running = POP_FALSE;
                }

                // next sub instruction
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                break;
            // return
            default:
                //((POP_parsling__declaration*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_type = (POP_parsling__type*)return_value;
                return_value = stack.p_stack[stack.p_height].p_output_base_pointer;
                stack.p_height--;
                continue;
            }
            break;
        case POP_pt__name:
            switch (stack.p_stack[stack.p_height].p_sub_instruction_pointer) {
            // identifier
            case 0:
                // allocate space
                output_top += sizeof(POP_parsling__name);

                // check for lexeme
                if (lexemes.p_lexeme_types[lexeme_index] == POP_lt__identifier) {
                    ((POP_parsling__name*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_name_lexeme_index = lexeme_index;
                    lexeme_index++;
                } else {
                    // log error
                    error_log->p_error_trace[0] = POP_et__parsing_incomplete_name;
                    error_log->p_parsing_missing_lexeme = POP_lt__identifier;
                    error_log->p_parsing_lexeme_index = lexeme_index;

                    // quit
                    running = POP_FALSE;
                }

                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                break;
            // return
            default:
                return_value = stack.p_stack[stack.p_height].p_output_base_pointer;
                stack.p_height--;
                continue;
            }
            break;
        case POP_pt__type:
            switch (stack.p_stack[stack.p_height].p_sub_instruction_pointer) {
            // name
            case 0:
                // allocate space
                output_top += sizeof(POP_parsling__type);

                // next sub instruction after call
                stack.p_stack[stack.p_height].p_sub_instruction_pointer++;
                
                // call function
                stack.p_height++;
                stack.p_stack[stack.p_height].p_instruction = POP_pt__name;
                stack.p_stack[stack.p_height].p_sub_instruction_pointer = 0;
                stack.p_stack[stack.p_height].p_output_base_pointer = output_top;

                break;
            // return
            default:
                ((POP_parsling__type*)stack.p_stack[stack.p_height].p_output_base_pointer)->p_type_name = (POP_parsling__name*)return_value;
                return_value = stack.p_stack[stack.p_height].p_output_base_pointer;
                stack.p_height--;
                continue;
            }
            break;
        default:
            // force return, instruction not implemented
            return_value = 0;
            stack.p_height--;
            break;
        }
    }

    // destroy stack
    free(stack.p_stack);

    return output;
}

void POP_print_empty(unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("empty\n");

    return;
}

void POP_print_lexeme_index(char* lexeme_name, unsigned long long lexeme_index, unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("%s: %llu\n", lexeme_name, lexeme_index);

    return;
}

void POP_print_parsling__name(POP_parsling__name* name, unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("name:\n");

    if (name != 0) {
        POP_print_lexeme_index("identifier", name->p_name_lexeme_index, tab_depth + 1);
    } else {
        POP_print_empty(tab_depth + 1);
    }

    return;
}

void POP_print_parsling__type(POP_parsling__type* type, unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("type:\n");

    if (type != 0) {
        POP_print_parsling__name(type->p_type_name, tab_depth + 1);
    } else {
        POP_print_empty(tab_depth + 1);
    }

    return;
}

void POP_print_parsling__declaration(POP_parsling__declaration* declaration, unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("declaration:\n");
    
    if (declaration != 0) {
        // print name
        POP_print_parsling__name(declaration->p_name, tab_depth + 1);
        
        // print colon
        POP_print_lexeme_index("colon", declaration->p_colon_lexeme_index, tab_depth + 1);

        // print type
        POP_print_parsling__type(declaration->p_type, tab_depth + 1);

        // print equals
        POP_print_lexeme_index("equals", declaration->p_equals_lexeme_index, tab_depth + 1);
    } else {
        POP_print_empty(tab_depth);
    }

    return;
}

void POP_print_parsling__declarations(POP_parsling__declarations* declarations, unsigned long long tab_depth) {
    POP_print_tabs(tab_depth);
    printf("declarations:\n");
        
    if (declarations != 0) {
        while (declarations != 0) {
            POP_print_parsling__declaration(declarations->p_declaration, tab_depth + 1);

            declarations = declarations->p_declarations;
        }
    } else {
        POP_print_empty(tab_depth + 1);
    }

    return;
}

void POP_print_parsling__file(POP_parsling__file* file, unsigned long long tab_depth) {
    // print
    POP_print_tabs(tab_depth);
    printf("file:\n");

    // if file is not empty
    if (file != 0) {
        POP_print_parsling__declarations(file->p_declarations, tab_depth + 1);
    } else {
        POP_print_empty(tab_depth + 1);
    }

    return;
}

void POP_print_parslings(POP_buffer parslings) {
    printf("Parslings:\n");
    unsigned long long tab_depth = 1;
    
    // print the file
    POP_print_parsling__file((POP_parsling__file*)parslings.p_data, tab_depth);

    // close off the end
    printf("----------\n");

    return;
}

/* Main */
int main() {
    POP_error_log error_log = POP_initialize_error_log();
    POP_buffer file;
    POP_code_points code_points;
    POP_lexemes lexemes;
    POP_buffer parslings;
    int return_code = 0;

    // tell the user that compilation has started
    printf("Starting Compilation!\n");

    // load the first file
    file = POP_load_file_into_buffer(&(error_log.p_error_trace[0]), (char*)"./prog/basic.pop");
    if (POP_error_occured(error_log) == POP_TRUE) {
        POP_print_error_log(error_log);

        return_code = 1;

        if (error_log.p_error_trace[0] == POP_et__loading_file_could_not_be_fully_read) {
            goto POP_label__destroy_file;
        } else {
            goto POP_label__return;
        }
    }

    // print the file to the console
    POP_print_buffer(file);

    // extract the code points from the file
    code_points = POP_deserialize_utf8(&(error_log.p_error_trace[0]), file);
    if (POP_error_occured(error_log) == POP_TRUE) {
        POP_print_error_log(error_log);

        return_code = 2;

        goto POP_label__destroy_file;
    }

    // print code points to the console
    POP_print_code_points(code_points);

    // lex the code points
    lexemes = POP_lex_code_points(&(error_log.p_error_trace[0]), code_points);
    if (POP_error_occured(error_log) == POP_TRUE) {
        POP_print_error_log(error_log);

        return_code = 3;

        goto POP_label__destroy_code_points;
    }

    // print the lexemes to the console
    POP_print_lexemes(lexemes);

    // parse the lexemes
    parslings = POP_parse_lexemes(&error_log, lexemes);
    if (POP_error_occured(error_log) == POP_TRUE) {
        POP_print_error_log(error_log);

        return_code = 4;

        goto POP_label__destroy_parslings;
    }

    POP_print_parslings(parslings);

    // tell the user that compilation has finished & flush any remaining text
    printf("Compilation Successful!\n");
    fflush(stdout);

    // cleanup
    POP_label__destroy_parslings: POP_destroy_buffer(parslings);
    POP_destroy_lexemes(lexemes);
    POP_label__destroy_code_points: POP_destroy_code_points(code_points);
    POP_label__destroy_file: POP_destroy_buffer(file);

    // test complete
    POP_label__return: return return_code;
}